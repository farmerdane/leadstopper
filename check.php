<?php

include_once("vendor/autoload.php");
include_once(__DIR__."/inc/checks.php");
include_once(__DIR__."/inc/LeadStopper.php");

$ls = new LeadStopper($checks);
$ls->runAllChecks();

