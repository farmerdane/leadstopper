<?php

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;

class LeadStopper
{

    const BASE_URL = 'https://graph.facebook.com/v3.2/';
    const FB_VERIFY_TOKEN = '';
    const FB_ACCESS_TOKEN = '';

    public function __construct($checks)
    {
        $this->checks = $checks;
        $this->initDB();
    }

    private function initDB()
    {
        $factory = new \Database\Connectors\ConnectionFactory();
        $this->conn = $factory->make(array(
            'driver' => 'mysql',
            'host' => '',
            'username' => '',
            'password' => '',
            'database' => 'leads',
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'lazy' => true,
            'options' => array(
                PDO::MYSQL_ATTR_LOCAL_INFILE => true,
                PDO::ATTR_EMULATE_PREPARES => true,
            ),
        ));
    }

    private function getLeadCount($check)
    {
        $statement = $this->conn->query(
            'SELECT count(id) as ad_count FROM leads WHERE (lead_time BETWEEN ? AND ?) AND ad_id = ?',
            array($check['start'], $check['end'], $check['ad_id'])
        );
        $res_agg = $statement->fetchAll(PDO::FETCH_ASSOC);
        return (count($res_agg) > 0) ? (int) $res_agg[0]['ad_count'] : 0;
    }

    private function insertLead($input)
    {
        //insertion data will vary based on campaign requirements/structure/aggregations needed
        $ping = $input['entry'][0]['changes'];

        if (count($ping) <= 0) {
            return;
        }

        $insertion = [];
        $runchecks = [];
        foreach ($ping as $p) {
            if ($p['field'] !== 'leadgen') {
                return;
            }

            $ad_id = $p['value']['ad_id'];
            $insertion[] = [
                'ad_id' => $ad_id,
                'lead_id' => $p['value']['leadgen_id'],
                'lead_time' => date('Y-m-d G:i:s', $p['value']['created_time']),
            ];

            if (isset($this->checks[$ad_id])) {
                $runchecks[] = $this->checks[$ad_id];
            }
        }

        if (count($insertion)) {
            $this->conn->table('leads')->insert($insertion);
            $this->runChecks($runchecks);
        }
    }

    public function checkLeadCount($check)
    {
        $ad_count = $this->getLeadCount($check);
        if ($ad_count >= $check['max_leads']) {
            $this->sendPauseRequest($check['ad_id']);
        }
    }

    public function runAllChecks()
    {
        $this->runChecks($this->checks);
    }

    public function runChecks($checks)
    {
        foreach ($checks as $check) {
            $this->checkLeadCount($check);
            sleep(1);
        }
    }

    public function handleRequest()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            if (
                isset($_GET['hub_mode']) && $_GET['hub_mode'] === 'subscribe' &&
                isset($_GET['hub_challenge']) &&
                isset($_GET['hub_verify_token']) && SELF::FB_VERIFY_TOKEN === $_GET['hub_verify_token']
            ) {
                echo $_GET['hub_challenge'];
                exit;
            }

        } else if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $jsonbody = file_get_contents('php://input');
            $input = json_decode($jsonbody, true);
            $this->insertLead($input);
            file_put_contents(__DIR__ . '/../log/log.log', json_encode($input) . "\r\n", FILE_APPEND);
        }

        echo ">:|";
    }

    public function sendPauseRequest($ad_id)
    {

        $client = new Client([
            'timeout' => 20.0,
        ]);

        try {
            $response = $client->request('POST', SELF::BASE_URL . $ad_id, [
                'query' => [
                    "status" => 'PAUSED',
                    "access_token" => SELF::FB_ACCESS_TOKEN,
                ],
            ]);
            $res = (string) $response->getBody();
            file_put_contents(__DIR__ . '/../log/logsuccess.log', json_encode([
                'ad_id' => $ad_id,
                'res' => $res,
                'ts' => time(),
            ]) . "\r\n", FILE_APPEND);
        } catch (RequestException $e) {
            $msg = [];
            $msg['req'] = Psr7\str($e->getRequest());
            if ($e->hasResponse()) {
                $msg['res'] = Psr7\str($e->getResponse());
            }
            file_put_contents(__DIR__ . '/../log/logerrs.log', json_encode($msg) . "\r\n", FILE_APPEND);
            return false;
        }
    }
}
